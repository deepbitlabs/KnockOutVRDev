﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(EnemyAI))]
public class EnemyController : MonoBehaviour
{

    private UnityEngine.AI.NavMeshAgent navAgent;
    public Animator animator;
    private EnemyAI enemyAI;
    public Transform target;
    public Transform lookTarget;
    public float punchingDistance = 1;
    public bool ragdol = false;

    // Enemy Health
    public float maxHealth = 100f;
    public float curHealth;


    private Vector3 lastPosition;
    private Dictionary<string, int> hitTriggers;
    private Vector3 smoothDeltaPosition = Vector2.zero;
    public bool RootMotionOn = false;
    public float invulnerabilityTime = 0.1f;
    bool isDead;

	// GameManager
	public GameController gameController;

    void Start()
    {
        lastPosition = transform.position;
        navAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        navAgent.updateRotation = false;
        animator = GetComponent<Animator>();
        enemyAI = GetComponent<EnemyAI>();
        hitTriggers = new Dictionary<string, int>();

        // Enemy Health
        curHealth = maxHealth;

    }

    float CalculateHealth()
    {
        return curHealth / maxHealth;
    }

    void TakeDamage(float damageValue)
    {
        curHealth -= damageValue;
    }

    // Update is called once per frame
    void Update()
    {

		if (curHealth <= 10f) 
		{
            Debug.Log("Enemy is hurt!");

            if (curHealth <= 0f)
            {
                if (!isDead)
                {
                    killEnemy();
                }
            }
		} 

        if (!RootMotionOn)
        {
            FaceTarget();
        }

        if (ragdol)
        {
            ragdol = false;
            RagdolSkeleton();
        }
    }

    public void UpdateDestination()
    {
        Vector3 destination = transform.position - target.position;
        destination = destination.normalized * punchingDistance;
        navAgent.SetDestination(destination);
    }

    public void UpdateAnimator()
    {
        Vector3 localVelocity = transform.InverseTransformDirection(navAgent.desiredVelocity);
        animator.SetFloat("VelX", localVelocity.x);
        animator.SetFloat("VelZ", localVelocity.z);
    }

    private void FaceTarget()
    {
        Vector3 targetPostition = new Vector3(lookTarget.position.x,
                                        this.transform.position.y,
                                        lookTarget.position.z);
        this.transform.LookAt(targetPostition);
    }

    private void RagdolSkeleton()
    {
		animator.enabled = true;
        navAgent.Stop();
        SetKinematicInChildren(false);
    }

    private void SetKinematicInChildren(bool isKinematic)
    {
        Rigidbody[] rigidBodies = GetComponentsInChildren<Rigidbody>();
        foreach (var rigidbody in rigidBodies)
        {
            rigidbody.isKinematic = isKinematic;
        }
    }

    private void SetJointBreakForce(float breakForce)
    {
        CharacterJoint[] joints = GetComponentsInChildren<CharacterJoint>();
        foreach (var joint in joints)
        {
            joint.breakForce = breakForce;
        }
    }


    public void StartRigidMotionAnimation()
    {
        if (!RootMotionOn)
        {
            RootMotionOn = true;
            navAgent.Stop();
            animator.SetFloat("VelX", 0f);
            animator.SetFloat("VelZ", 0f);
            animator.applyRootMotion = true;
        }

    }
    public void FinishRigidMotionAnimation()
    {
        if (RootMotionOn)
        {
            RootMotionOn = false;
            animator.applyRootMotion = false;
            navAgent.Resume();
        }
    }

    public bool IsAtDestination()
    {
        bool atTraget = false;
        if (!navAgent.pathPending)
        {
            if (navAgent.remainingDistance <= navAgent.stoppingDistance)
            {
                if (!navAgent.hasPath || navAgent.velocity.sqrMagnitude == 0f)
                {
                    atTraget = true;
                }
            }
        }
        return atTraget;
    }

    public void ExecuteAttack()
    {
        string attack = PickAttack();
        animator.SetTrigger(attack);
    }

    public string PickAttack()
    {
        int attackNumber = UnityEngine.Random.Range(0, 7);
        switch (attackNumber)
        {
            case 0: return "StrLeft";
            case 1: return "StrRight";
            case 2: return "HookLeft";
            case 3: return "Combo2";
			case 4: return "Combo1";
			case 5: return "Combo3";
			case 6: return "RightRH";
			case 7: return "LeftRH";
            default: return "StrLeft";
        }
    }

	public void ExecuteDefend()
	{
		string defend = PickDefend ();
		animator.SetTrigger (defend);
	}

	public string PickDefend()
	{
		int defendNumber = UnityEngine.Random.Range (0, 2);
		switch (defendNumber) 
		{
		case 0:
			return "BlockHigh";
		case 1:
			return "BlockLow";
		case 2:
			return "BlockHigh";
		default: return "BlockHigh";
		}
	}

//	public void ExecuteCelebration()
//	{
//		string celebration = PickCelebration ();
//		animator.SetTrigger (celebration);
//		
//	}
//
//	public string PickCelebration()
//	{
//		int CelebrationNumber = UnityEngine.Random.Range (0, 2);
//		switch (CelebrationNumber) 
//		{
//		case 0:
//			return "BlockHigh";
//		case 1:
//			return "BlockLow";
//		case 2:
//			return "BlockHigh";
//		default: return "BlockHigh";
//		}
//	}

    public void killEnemy()
    {
        ragdol = true;

		// Displaying victory menu when player wins the fight
		gameController.VictoryMenu.SetActive(true);

		// Destroying gameobject when player dies
		Destroy(gameObject, 5);

    }
}
