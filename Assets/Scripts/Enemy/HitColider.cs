﻿using UnityEngine;
using System.Collections;

public class HitColider : MonoBehaviour
{
	public PlayerController player;

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "RHitCollider" || col.gameObject.tag == "LHitCollider")
		{
			if (gameObject.tag == "Player") 
			{
				Debug.Log(" Player has been hit! ");
				player.curHealth -= 0.01f;
				//mBlur.enabled = true;
			}
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "RHitCollider" || col.gameObject.tag == "LHitCollider")
		{
			if (gameObject.tag == "Player") 
			{
				Debug.Log(" Player has not been hit! ");
				return;
			}
		}
	}
}
