﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.ImageEffects;

public class PlayerController : MonoBehaviour
{
    // Player Health
    public float maxHealth = 100f;
    public float curHealth;

    // Player is dead
    bool isDead;


    // GameManager
	public GameController gameController;
	private EnemyController enemy;

    void Start()
    {
        gameController = FindObjectOfType<GameController>();
        curHealth = maxHealth;


		// Motion Blur effect
		GetComponent<UnityStandardAssets.ImageEffects.MotionBlur>().enabled = false;
    }

    void Update()
    {

        if (curHealth <= 0)
        {
            if (!isDead)
            {
                killPlayer();
            }
        }
    }

    void TakeDamage(float amount)
    {
        curHealth -= amount;
    }

    public void killPlayer()
    {
        Debug.Log(" Player is dead! ");
		gameController.DefeatMenu.SetActive (true);
    }

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "RHitCollider" || col.gameObject.tag == "LHitCollider")
		{
			if (gameObject.tag == "Player") 
			{
				Debug.Log(" Player has been hit! ");
				curHealth -= 0.01f;
				GetComponent<UnityStandardAssets.ImageEffects.MotionBlur>().enabled = true;
			}
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "RHitCollider" || col.gameObject.tag == "LHitCollider")
		{
			if (gameObject.tag == "Player") 
			{
				Debug.Log(" Player has not been hit! ");
				GetComponent<UnityStandardAssets.ImageEffects.MotionBlur>().enabled = false;
				return;
			}
		}
	}
}
