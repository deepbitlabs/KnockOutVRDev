﻿using UnityEngine;
using System.Collections;

public class PhysicsProperties : MonoBehaviour {

    public float gloveForceMultiplier = 50f;
    public Vector3 gloveSpeed = Vector3.zero;

    private SteamVR_TrackedObject trackedObject;

    void FixedUpdate()
    {
        if(this.transform.parent != null && trackedObject == null)
        {
            trackedObject = GetComponentInParent<SteamVR_TrackedObject>();
        }
        if(trackedObject != null)
        {
            SteamVR_Controller.Device device = SteamVR_Controller.Input((int)trackedObject.index);

            gloveSpeed = device.velocity;
        }
    }
}
