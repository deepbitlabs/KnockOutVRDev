﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour 
{
	public bool isStarted = false;

    public bool isGameOver = false;

	private int levelSelectIndex = 5;

	private int menuIndex = 3;

	public GameObject VictoryMenu;
	public GameObject DefeatMenu;

	public int level;

	void Awake()
	{
		VictoryMenu.SetActive (false);
		DefeatMenu.SetActive (false);

		// Restarting the current active level
		level = SceneManager.GetActiveScene ().buildIndex;
	}

	public void StartGame()
	{
		isStarted = true;
	}
		
	public void DisplayGameOver()
	{
		DefeatMenu.SetActive (true);
	}

	public void DisplayVictory()
	{
		VictoryMenu.SetActive (true);
	}
}
