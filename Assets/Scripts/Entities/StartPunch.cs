﻿using UnityEngine;
using System.Collections;

public class StartPunch : MonoBehaviour 
{

	public GameController gameController;


    void Start()
    {
		gameController = FindObjectOfType<GameController>();
    }

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Left" || col.gameObject.tag == "Right") 
		{
			if (gameObject.tag == "RHitCollider" || gameObject.tag == "LHitCollider") 
			{
				gameController.StartGame ();
			}
		}
	}
}
